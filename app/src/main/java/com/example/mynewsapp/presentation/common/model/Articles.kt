package com.example.mynewsapp.presentation.common.model

data class Articles(
     var publishedAt: String? = null,
     var author: String? = null,
     var urlToImage: String? = null,
     var description: String? = null,
     var title: String? = null,
     var url: String? = null,
     var content: String? = null
)