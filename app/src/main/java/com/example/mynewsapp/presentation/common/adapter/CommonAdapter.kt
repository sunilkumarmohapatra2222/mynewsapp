package com.example.mynewsapp.presentation.common.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mynewsapp.R
import com.example.mynewsapp.presentation.common.model.Articles
import kotlinx.android.synthetic.main.row_news.view.*

class CommonAdapter(private var activity: FragmentActivity, private val onItemClick: (dataBean: Articles) -> Unit ) :
    RecyclerView.Adapter<CommonAdapter.CommonViewHolder>() {
    private var commonList = ArrayList<Articles>()

    fun setList(arrayList: ArrayList<Articles>) {
        this.commonList = arrayList
        notifyDataSetChanged()
    }
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CommonViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_news, parent, false)
        return CommonViewHolder(view)
    }

    override fun getItemCount(): Int {
        return commonList.size
    }

    override fun onBindViewHolder(holder: CommonViewHolder, position: Int) {
        val itemView = holder.itemView
        val commonModel = commonList[position]
        itemView.newsTitle.text = commonModel.title
        Glide
            .with(activity)
            .load(commonModel.urlToImage)
            .into(itemView.newsImage)
        itemView.setOnClickListener{
            onItemClick.invoke(commonModel)
        }
    }
    inner class CommonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}