package com.example.mynewsapp.presentation.common.utils


class Resource<T>(val status: Status) {
    var item: T? = null
    constructor(status: Status, item: T) : this(status) {
        this.item = item
    }
}
