package com.example.mynewsapp.presentation.common.news

import android.content.Intent
import android.view.View
import androidx.lifecycle.ViewModelProviders
import com.example.mynewsapp.R
import com.example.mynewsapp.presentation.common.adapter.CommonAdapter
import com.example.mynewsapp.presentation.common.model.Articles
import com.example.mynewsapp.presentation.common.common.BaseViewModelFragment
import com.example.mynewsapp.presentation.common.common.SafeObserver
import com.example.mynewsapp.presentation.common.utils.*
import com.example.mynewsapp.presentation.common.webview.NewsDetailsActivity
import kotlinx.android.synthetic.main.common_fragment.*

class CommonNewsFragment(var value: Int) : BaseViewModelFragment<CommonNewsViewModel>() {
    private var isLoading = false
    private val commonAdapter by lazy {
        CommonAdapter(
            requireActivity(),
            ::onItemClick
        )
    }

    override fun buildViewModel(): CommonNewsViewModel {
        return ViewModelProviders.of(this)[CommonNewsViewModel::class.java]
    }

    override fun getContentResource(): Int = R.layout.common_fragment

    override fun initLiveDataObservers() {
        super.initLiveDataObservers()
        viewModel.articlesLiveData.observe(this, SafeObserver(this::handleLeaderBoardData))
    }

    override fun initViews() {
        super.initViews()
        when (value) {
            1 -> {
                viewModel.callArticlesApi(SPORTS)
            }
            2 -> {
                viewModel.callArticlesApi(SCIENCE)
            }
            3 -> {
                viewModel.callArticlesApi(ENTERTAINMENT)
            }
            4 -> {
                viewModel.callArticlesApi(TECHNOLOGY)
            }
            5 -> {
                viewModel.callArticlesApi(BUSINESS)
            }
        }
    }

    private fun handleLeaderBoardData(response: Resource<MutableList<Articles>>) {
        isLoading = false
        response.let {
            when (response.status) {
                Status.LOADING -> progressBar.visibility = View.VISIBLE
                Status.ERROR -> handleError()
                Status.SUCCESS -> handleSuccess(it.item)
            }
        }
    }

    private fun handleSuccess(item: MutableList<Articles>?) {
        progressBar.visibility = View.GONE
        rvNews.adapter = commonAdapter
        commonAdapter.setList(item as ArrayList<Articles>)
    }

    private fun handleError() {
        progressBar.visibility = View.GONE
    }

    private fun onItemClick(dataBean: Articles) {
        val intent = Intent(requireActivity(), NewsDetailsActivity::class.java)
        intent.putExtra(URl, dataBean.url)
        startActivity(intent)
    }
}