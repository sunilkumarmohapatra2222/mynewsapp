package com.example.mynewsapp.presentation.common.news


import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class NewsAdapter(
    manager: FragmentManager
) : FragmentStatePagerAdapter(manager,BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val tabNames: ArrayList<String> = ArrayList()

    private val fragments: ArrayList<Fragment> = ArrayList()

    fun add(frag: List<Fragment>, titles: List<String>) {
        tabNames.addAll(titles)
        fragments.addAll(frag)
    }

    override fun getCount(): Int {
        return fragments.size
    }

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getPageTitle(position: Int): CharSequence {
        return tabNames[position]
    }
}