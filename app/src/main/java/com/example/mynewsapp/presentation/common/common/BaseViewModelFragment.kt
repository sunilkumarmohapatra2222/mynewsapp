package com.example.mynewsapp.presentation.common.common

import android.os.Bundle

import androidx.annotation.CallSuper

abstract class BaseViewModelFragment<T : BaseViewModel> : BaseFragment() {
    val viewModel by lazy { buildViewModel() }
    protected abstract fun buildViewModel(): T
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initLiveDataObservers()
    }

    @CallSuper
    protected open fun initLiveDataObservers() {
    }
}