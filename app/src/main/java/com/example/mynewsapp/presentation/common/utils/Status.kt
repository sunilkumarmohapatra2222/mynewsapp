package com.example.mynewsapp.presentation.common.utils

enum class Status {
    SUCCESS,
    LOADING,
    ERROR
}