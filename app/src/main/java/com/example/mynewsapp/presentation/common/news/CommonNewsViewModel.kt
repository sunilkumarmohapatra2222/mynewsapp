package com.example.mynewsapp.presentation.common.news

import androidx.lifecycle.MutableLiveData
import com.example.mynewsapp.presentation.common.apiconfig.WebApiClient
import com.example.mynewsapp.presentation.common.model.ArticleResponse
import com.example.mynewsapp.presentation.common.model.Articles
import com.example.mynewsapp.presentation.common.utils.Resource
import com.example.mynewsapp.presentation.common.utils.Status
import com.example.mynewsapp.presentation.common.common.BaseViewModel
import com.example.mynewsapp.presentation.common.utils.APIKEY
import com.example.mynewsapp.presentation.common.utils.COUNTRY
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CommonNewsViewModel : BaseViewModel() {
    val articlesLiveData = MutableLiveData<Resource<MutableList<Articles>>>()

    fun callArticlesApi(category: String) {
        articlesLiveData.value = Resource(Status.LOADING)
        WebApiClient.webApi
            .articlesAPI(
                apikey = APIKEY,
                category = category,
                country = COUNTRY
            )
            .enqueue(object : Callback<ArticleResponse> {
                override fun onResponse(
                    call: Call<ArticleResponse>?,
                    response: Response<ArticleResponse>?
                ) {
                    if (response != null) {
                        if (response.isSuccessful) {
                            if (response.body() != null) {
                                response.body()?.articles.let {
                                    articlesLiveData.value = Resource(Status.SUCCESS, it!!)
                                }
                            } else {
                                articlesLiveData.value = Resource(Status.ERROR)
                            }
                        } else {
                            articlesLiveData.value = Resource(Status.ERROR)
                        }
                    }
                }
                override fun onFailure(call: Call<ArticleResponse>?, t: Throwable?) {
                    t?.printStackTrace()
                    articlesLiveData.value = Resource(Status.ERROR)
                }
            })
    }
}