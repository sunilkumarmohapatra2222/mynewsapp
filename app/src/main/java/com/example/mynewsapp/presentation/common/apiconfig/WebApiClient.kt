package com.example.mynewsapp.presentation.common.apiconfig

import com.example.mynewsapp.BuildConfig
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class WebApiClient {
    companion object {
        val webApi: WebApi by lazy {
            val logging = getLoggingInstance()
            val client = OkHttpClient.Builder()
                .readTimeout(5, TimeUnit.MINUTES)
                .connectTimeout(5, TimeUnit.MINUTES)
                .addInterceptor { chain ->
                    val request = chain.request().newBuilder()
                        .addHeader("Accept", "application/json")
                        .build()
                    chain.proceed(request)
                }.addInterceptor(logging).build()
            val retrofit = retrofitInstance(client)

            retrofit.create(WebApi::class.java)
        }


        private fun retrofitInstance(client: OkHttpClient): Retrofit {
            return Retrofit.Builder()
                .baseUrl("http://newsapi.org/v2/")
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
        private fun getLoggingInstance(): HttpLoggingInterceptor {
            val logging = HttpLoggingInterceptor()
            if (BuildConfig.DEBUG) {
                logging.level = HttpLoggingInterceptor.Level.BODY
            } else {
                logging.level = HttpLoggingInterceptor.Level.NONE
            }
            return logging
        }
    }
}






