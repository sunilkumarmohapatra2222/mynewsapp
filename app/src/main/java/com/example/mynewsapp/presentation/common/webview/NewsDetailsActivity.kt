package com.example.mynewsapp.presentation.common.webview

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Build
import android.view.View
import android.webkit.*
import androidx.annotation.RequiresApi
import com.example.mynewsapp.R
import com.example.mynewsapp.presentation.common.common.BaseActivity
import com.example.mynewsapp.presentation.common.utils.URl
import kotlinx.android.synthetic.main.frgament_news_details.*

class NewsDetailsActivity : BaseActivity() {
    var url:String?=""
    lateinit var cookieManager: CookieManager

    private fun getParcableData() {
        val bundle = intent.extras
        if (bundle != null) {
            if (bundle.containsKey(URl)) {
                url = bundle.getString(URl)
            }
        }
    }
    override fun getContentResource(): Int = R.layout.frgament_news_details


    @SuppressLint("SetJavaScriptEnabled")
    override fun initViews() {
        super.initViews()
        getParcableData()
        pbLoader.visibility = View.VISIBLE
        cookieManager = CookieManager.getInstance()
        cookieManager.setAcceptCookie(true)
        cookieManager.removeAllCookie()
        val db = WebViewDatabase.getInstance(this)
        db.clearFormData()
        wvTwitch.webViewClient = MyWebClient()
        wvTwitch.loadUrl(url)
    }
    inner class MyWebClient : WebViewClient() {
        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            cookieManager.setAcceptCookie(true)
            pbLoader.visibility = View.VISIBLE
        }

        @SuppressWarnings("deprecation")
        override fun shouldOverrideUrlLoading(view: WebView?, url: String): Boolean {
            return handleUrl(url)
        }

        @RequiresApi(Build.VERSION_CODES.N)
        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            return handleUrl(request?.url.toString())
        }

        private fun handleUrl(url: String): Boolean {
            wvTwitch.loadUrl(url)
            return true
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            pbLoader.visibility = View.GONE
            view?.settings?.useWideViewPort = false
        }
    }
}

