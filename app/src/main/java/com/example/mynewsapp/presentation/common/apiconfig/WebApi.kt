package com.example.mynewsapp.presentation.common.apiconfig

import com.example.mynewsapp.presentation.common.model.ArticleResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface WebApi {
    @GET("top-headlines")
    fun articlesAPI(
        @Query("country") country: String,
        @Query("category") category: String,
        @Query("apiKey") apikey: String
    ): Call<ArticleResponse>
}
