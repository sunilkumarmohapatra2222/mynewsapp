package com.example.mynewsapp
import androidx.fragment.app.Fragment
import com.example.mynewsapp.presentation.common.news.NewsAdapter
import com.example.mynewsapp.presentation.common.news.CommonNewsFragment
import com.example.mynewsapp.presentation.common.common.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {
    private var tabNewsList: MutableList<String> = mutableListOf()
    private var fragments: MutableList<Fragment> = mutableListOf()
    private val adapter by lazy {
        NewsAdapter(supportFragmentManager)
    }
    override fun getContentResource(): Int = R.layout.activity_main

    override fun initViews() {
        super.initViews()
        setTabLayout()
    }

    private fun setTabLayout() {
        tabNewsList = this.resources.getStringArray(R.array.tabList).toMutableList()
        val list: MutableList<String> = ArrayList()
        for (i in 0 until tabNewsList.size) {
            list.add(tabNewsList[i])
            when (tabNewsList[i]) {
                resources.getString(R.string.sports) -> {
                    fragments.add(CommonNewsFragment(1))
                }
                resources.getString(R.string.science) -> {
                    fragments.add(CommonNewsFragment(2))
                }
                resources.getString(R.string.entertment) -> {
                    fragments.add(CommonNewsFragment(3))
                }
                resources.getString(R.string.technology) -> {
                    fragments.add(CommonNewsFragment(4))
                }
                resources.getString(R.string.business) -> {
                    fragments.add(CommonNewsFragment(5))
                }
                else -> {
                    fragments.add(CommonNewsFragment(1))
                }
            }
        }
        adapter.add(fragments, list)
        viewpager.adapter = adapter
        tlLayout.setupWithViewPager(viewpager)
    }
}
